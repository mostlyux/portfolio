import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import './globals.css'
import './layout.css'
import Header from '../header/header'
import Footer from '../footer'
import MobileMenu from '../menu-mobile'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Portfolio site of Mike Nichols, a UX designer and product development lead.' },
            { name: 'keywords', content: 'ux, ui, user experience, product development, ux consultant' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1.0'}
          ]}
        >
          <html lang="en" />
          <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"></link>
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"></link>
        </Helmet>
        <MobileMenu />
        <Header />
        <div
          style={{
            margin: '0 auto',
            padding: '96px 0 0 0',
            minHeight: '80vh',
          }}
        >
          {children}
        </div>
        <Footer />
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
