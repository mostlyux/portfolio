import React from 'react'

//import compStyle from './css/nielsen.module.css'
import Layout from '../../components/layout/layout'

import HeaderImg from '../../images/bishop/bishop-intro.jpg'
import SecImg1 from '../../images/bishop/bishop-one-button.jpg'
import SecImg2 from '../../images/bishop/bishop-employee-flow.jpg'
import SecImg3 from '../../images/bishop/bishop-employee-screens.jpg'
import SecImg4 from '../../images/bishop/bishop-supervisor.jpg'
import SecImg5 from '../../images/bishop/bishop-supervisor-flow.jpg'
import SecImg6 from '../../images/bishop/bishop-popup.gif'
import SecImg7 from '../../images/bishop/bishop-supervisor-screens.jpg'
import SecImg8 from '../../images/bishop/bishop-result.jpg'

const Blog3 = () => (
  <Layout>
    <section className='section-double section-intro'>
      <div className='section-content'>
        <h3>Bishop</h3>
        <h2>Web/Mobile App Design</h2>
        <p>
        Bishop specializes in commercial and residential cleaning services.  I was contracted to design an app to help them manage 
        their employees and the locations they service. The design included a web app for supervisors, a mobile app for employees 
        to use while on location, and a mobile app for supervisors to monitor when away from the office.
        </p>
      </div>
      <div className='section-img'>
        <img src={HeaderImg} alt="Nielsen Screen" />
      </div>
    </section>
    <section className='section-double section-reverse'>
      <div className='section-content'>
        <h3>Employee Flow</h3>
        <h2>One Button Simple</h2>
        <p>
        For the employee mobile app flow, simplicity and ease-of-use were at the top of the list for the design.  The app needed to 
        reduce employee training time not add to it.   To accomplish this, a step by step ‘one button approach’ was taken.  This 
        reduced cognitive load, allowed employees to focus on the task at hand and provided a clear path through the process.
        </p>
      </div>
      <div className='section-img'>
        <img src={SecImg1} alt="Bishop one button" />  
      </div>
    </section>
    <section className='section-single-image'>
      <img src={SecImg2} alt="Bishop employee flowmap" /> 
    </section>
    <section className='section-double section-reverse'>
      <div className='section-content'>
        <h3>Employee Flow</h3>
        <h2>Features</h2>
        <p>
        To-do list, image uploads, comments, and a message board for each location allow employees to communicate with each 
        other as well as with supervisors.
        </p>
      </div>
      <div className='section-img'>
        <img src={SecImg3} alt="Nielsen UX Design" /> 
      </div>
    </section>
    <section className='section-double'>
      <div className='section-content'>
        <h3>Supervisor Flow</h3>
        <h2>Digital Management </h2>
        <p>
        The supervisor web app was driven by business needs.  Here completeness, informational display, and speedy navigation were 
        the design focus.  The design was formed by working closely with supervisors to understand the tasks they perform.
        </p>
      </div>
      <div className='section-img'>
        <img src={SecImg4} alt="Bishop supervisor dashboard" />  
      </div>
    </section>
    <section className='section-single-image'>
      <img src={SecImg5} alt="Bishop supervisor flowmap" /> 
    </section>
    <section className='section-double'>
      <div className='section-content'>
        <h3>Supervisor Flow</h3>
        <h2>Material Dashboard</h2>
        <p>
        Material design inspired expandable cards were used to design a dashboard that allowed supervisors to have the necessary information available and 
        also jump in and out of tasks quickly.  
        </p>
      </div>
      <div className='section-img'>
        <img src={SecImg6} alt="Bishop Popup" />  
      </div>
    </section>
    <section className='section-single-image'>
        <br /><br />
      <img src={SecImg7} alt="Bishop supervisor screens" /> 
      <br /><br />
    </section>
    <section className='section-double'>
      <div className='section-content'>
        <h3>Result</h3>
        <h2>Business Transformation</h2>
        <p>
        While the desktop and mobile applications have different flows, experiences, and goals, they work together to provide a 
        system that aims to streamline the business, increase efficiency and reduce employee stress.  
        </p>
      </div>
      <div className='section-img'>
        <img src={SecImg8} alt="Bishop Result" />  
      </div>
    </section>

  </Layout>
)

export default Blog3

export const frontmatter = {
  title: "Bishop - UX Design",
  written: "02/24/2017",
  layoutType: "portfolio",
  category: "#ux #ui #design #project",
  path: 'p-bishop.jpg'
}